# Function to check if a given year is a leap year
def is_leap_year(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False

# Get year input from user
while True:
    try:
        year = int(input("Enter a year: "))
        if year <= 0:
            raise ValueError("Year must be a positive integer")
        break
    except ValueError:
        print("Invalid input. Please enter a positive integer for the year.")

# Check if the year is a leap year and print the result
if is_leap_year(year):
    print(year, "is a leap year.")
else:
    print(year, "is not a leap year.")

# Get row and column input from user
while True:
    try:
        row = int(input("Enter the number of rows: "))
        col = int(input("Enter the number of columns: "))
        if row <= 0 or col <= 0:
            raise ValueError("Row and column values must be positive integers")
        break
    except ValueError:
        print("Invalid input. Please enter positive integers for the row and column values.")

# Create grid of asterisks using row and column values
for i in range(row):
    for j in range(col):
        print("*", end="")
    print()
